package live.minitrack.config;

public class MinitrackConfig {

  public static String SERVER_URL = "https://beta.minitrack.live";
  public static boolean LOGGING_ENABLED = true;
}
