package live.minitrack.service;

import java.io.Serializable;

public record SendLocationContextData(
    String title, String key, String playerToken, Boolean fromCode) implements Serializable {}
