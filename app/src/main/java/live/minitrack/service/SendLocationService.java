package live.minitrack.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import live.minitrack.R;
import live.minitrack.activity.SendLocationActivity;
import live.minitrack.api.LogRequest;
import live.minitrack.api.MinitrackApi;
import live.minitrack.api.PlayerUpdate;
import live.minitrack.api.Position;
import live.minitrack.config.MinitrackConfig;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class SendLocationService extends Service {

  @Getter @Setter private String sharingCode;

  @Getter @Setter private boolean serviceRunning;

  private String key;
  private String playerToken;
  private String title;
  private Boolean fromCode;

  private LocationManager locationManager;
  private LocationListener locationListener;
  private MinitrackApi minitrackApi;

  private final CopyOnWriteArrayList<Position> positions = new CopyOnWriteArrayList<>();

  private final LocalBinder binder = new LocalBinder();

  private final Gson gson = (new GsonBuilder()).setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();

  private static final String NOTIFICATION_CHANNEL_ID = "Notification Minitrack";

  public int onStartCommand(@NotNull Intent intent, int flags, int startId) {
    Retrofit retrofit =
        (new Retrofit.Builder())
            .baseUrl(MinitrackConfig.SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create(this.gson))
            .build();
    this.minitrackApi = retrofit.create(MinitrackApi.class);
    if (VERSION.SDK_INT >= 26) {
      NotificationManager mngr =
          (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
      if (mngr.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
        NotificationChannel channel =
            new NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_ID,
                NotificationManager.IMPORTANCE_DEFAULT);
        mngr.createNotificationChannel(channel);
      }
    }

    SendLocationContextData contextData =
        (SendLocationContextData) intent.getSerializableExtra("context");
    this.key = contextData.key();
    this.playerToken = contextData.playerToken();
    this.title = contextData.title();
    this.fromCode = contextData.fromCode();
    LogRequest startLog =
        new LogRequest("INFO", "Starting location tracking", this.key, this.playerToken);
    this.sendLog(startLog);
    NotificationCompat.Builder builder =
        VERSION.SDK_INT >= 31
            ? (new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID))
                .setSmallIcon(R.drawable.lr_notification)
                .setContentTitle(this.getString(R.string.app_name))
                .setContentText("Envoi de la position")
                .setForegroundServiceBehavior(NotificationCompat.FOREGROUND_SERVICE_IMMEDIATE)
            : (new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID))
                .setSmallIcon(R.drawable.lr_notification)
                .setContentTitle(this.getString(R.string.app_name))
                .setContentText("Envoi de la position");

    Intent notifyIntent = new Intent(this, SendLocationActivity.class);
    notifyIntent.putExtra(
        "context",
        new SendLocationContextData(this.title, this.key, this.playerToken, this.fromCode));
    notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    PendingIntent notifyPendingIntent =
        PendingIntent.getActivity(
            this,
            0,
            notifyIntent,
            PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
    builder.setContentIntent(notifyPendingIntent);
    this.startForeground(1337, builder.build());
    ExecutorService executor = Executors.newSingleThreadExecutor();
    long delay = 15000L;
    executor.execute(
        () -> {
          while (true) {
            if (!this.positions.isEmpty()) {
              try {
                PlayerUpdate playerUpdate = new PlayerUpdate(this.playerToken, this.positions);
                this.sendLocationUpdate(this.key, playerUpdate);
              } catch (Throwable e) {
                this.key = e.getMessage();
                if (this.key == null) {
                  this.key = "";
                }

                Log.e("", this.key);
              }
            }

            try {
              Thread.sleep(delay);
            } catch (InterruptedException e) {
              throw new RuntimeException(e);
            }
          }
        });
    this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    boolean gpsEnabled = locationManager.isProviderEnabled("gps");
    if (!gpsEnabled) {
      Handler handlerLooper = new Handler(Looper.getMainLooper());
      handlerLooper.post(
          () -> Toast.makeText(this, R.string.gps_not_enabled, Toast.LENGTH_LONG).show());
    }

    this.locationListener = new MyLocationListener();
    if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") == 0
        && ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION")
            == 0) {
      if (this.locationManager != null) {
        this.locationManager.requestLocationUpdates("gps", 5000L, 0.0F, this.locationListener);
      }
    }
    this.serviceRunning = true;
    return START_STICKY;
  }

  private void sendLog(LogRequest log) {
    if (MinitrackConfig.LOGGING_ENABLED) {
      Call<String> call = this.minitrackApi.createLog(log);
      if (call != null) {
        call.enqueue(
            new Callback<>() {
              public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {}

              public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {}
            });
      }
    }
  }

  private void sendLocationUpdate(final String key, final PlayerUpdate playerUpdate) {
    Call<Void> call = this.minitrackApi.sendPlayerUpdate(key, playerUpdate);
    if (call != null) {
      call.enqueue(
          new Callback<>() {
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
              if (response.isSuccessful()) {
                SendLocationService.this.sendLog(
                    new LogRequest(
                        "INFO",
                        playerUpdate.positions().size() + " locations successfully sent",
                        key,
                        SendLocationService.this.playerToken));
                SendLocationService.this.positions.clear();
              }
            }

            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
              String errorMessage = "Unable to upload location : " + t.getMessage();
              SendLocationService.this.sendLog(
                  new LogRequest("ERROR", errorMessage, key, SendLocationService.this.playerToken));
              Log.e("", t.getMessage());
            }
          });
    }
  }

  public final class LocalBinder extends Binder {
    @NotNull
    public SendLocationService getService() {
      return SendLocationService.this;
    }
  }

  private final class MyLocationListener implements LocationListener {
    public MyLocationListener() {}

    public void onLocationChanged(@NotNull Location loc) {
      String key = SendLocationService.this.key;
      String playerToken = SendLocationService.this.playerToken;
      SendLocationService.this.sendLog(
          new LogRequest("INFO", "Location changed", key, playerToken));
      Position position =
          new Position(
              loc.getLongitude(),
              loc.getLatitude(),
              loc.getAltitude(),
              new Date(),
              loc.hasAccuracy() ? loc.getAccuracy() : 0.0F,
              loc.hasSpeed() ? loc.getSpeed() : 0.0F);
      SendLocationService.this.positions.add(position);
    }
  }

  public void onDestroy() {
    if (this.locationListener != null) {
      LocationManager locationManager = this.locationManager;
      if (locationManager != null) {
        LocationListener locationListener = this.locationListener;
        locationManager.removeUpdates(locationListener);
      }
    }
    this.sendLog(new LogRequest("INFO", "Service onDestroy called", this.key, this.playerToken));
  }

  @NotNull
  public IBinder onBind(@NotNull Intent intent) {
    return this.binder;
  }
}
