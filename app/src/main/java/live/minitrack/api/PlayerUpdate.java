package live.minitrack.api;

import java.util.List;

public record PlayerUpdate(String playerToken, List<Position> positions) {}
