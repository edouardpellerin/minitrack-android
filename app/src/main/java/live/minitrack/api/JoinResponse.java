package live.minitrack.api;

public record JoinResponse(String name, String playerToken, Map map) {}
