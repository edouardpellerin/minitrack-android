package live.minitrack.api;

public record MapRequest(String playerName, String title) {}
