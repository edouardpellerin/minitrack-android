package live.minitrack.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MinitrackApi {
  @POST("/api/v1/logs")
  Call<String> createLog(@Body LogRequest logRequest);

  @POST("/api/map/{key}/position")
  Call<Void> sendPlayerUpdate(@Path("key") String key, @Body PlayerUpdate playerUpdate);
}
