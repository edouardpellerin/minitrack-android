package live.minitrack.api;

public record LogRequest(String level, String message, String map, String token) {}
