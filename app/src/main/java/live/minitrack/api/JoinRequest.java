package live.minitrack.api;

public record JoinRequest(String name, String code) {}
