package live.minitrack.api;

import java.util.*;

public record Position(
    Double longitude,
    Double latitude,
    Double altitude,
    Date timestamp,
    Float accuracy,
    Float speed) {}
