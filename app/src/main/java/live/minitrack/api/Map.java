package live.minitrack.api;

public record Map(String title, String key) {}
