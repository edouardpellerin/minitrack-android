package live.minitrack.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PreferencesService {

  @Getter(lazy = true)
  private static final PreferencesService instance = new PreferencesService();

  public String getPlayerName(Context context) {
    return PreferenceManager.getDefaultSharedPreferences(context).getString("name", "");
  }

  public void updatePlayerName(Context context, String playerName) {
    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
    editor.putString("name", playerName);
    editor.apply();
  }
}
