package live.minitrack.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Random;
import kotlin.text.Regex;
import live.minitrack.R;
import live.minitrack.api.JoinRequest;
import live.minitrack.api.JoinResponse;
import live.minitrack.api.MapRequest;
import live.minitrack.config.MinitrackConfig;
import live.minitrack.preferences.PreferencesService;
import live.minitrack.service.SendLocationContextData;
import lombok.SneakyThrows;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.jetbrains.annotations.NotNull;

public final class MainActivity extends AppCompatActivity {

  private final PreferencesService preferencesService = PreferencesService.getInstance();

  private final Gson gson = new Gson();
  private final OkHttpClient client = new OkHttpClient();
  private final String[] images;

  public MainActivity() {
    this.images =
        new String[] {
          "alessio_soggetti_gyr9a2cpmhy_unsplash",
          "benjamin_huggett_dfu9ycyatyu_unsplash",
          "brian_erickson_xfnec_rhr48_unsplash",
          "chander_r_z4wh11fmfiq_unsplash",
          "greg_rosenke_e9pdtvfeuji_unsplash",
          "jenny_hill_mqvwb7kuooe_unsplash",
          "josh_gordon_fzhmp6z8oq4_unsplash",
          "marcos_paulo_prado_2csiupeysm0_unsplash"
        };
  }

  @SneakyThrows
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.activity_main);
    ImageView imageView = this.findViewById(R.id.imageView);
    Random random = new Random();
    int intImage = random.nextInt(this.images.length - 1);
    int id =
        this.getResources().getIdentifier(this.images[intImage], "drawable", this.getPackageName());
    imageView.setImageResource(id);

    String versionName =
        this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
    TextView versionTextView = this.findViewById(R.id.versionTextView);
    versionTextView.setText("v" + versionName);

    String playerName = preferencesService.getPlayerName(this);
    if (TextUtils.isEmpty(playerName)) {
      updatePlayerName();
    }

    Button createMapButton = this.findViewById(R.id.create_map);
    createMapButton.setOnClickListener(
        view -> {
          View dialogView = this.getLayoutInflater().inflate(R.layout.activity_enter_url, null);
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setView(dialogView);
          AlertDialog dialog = builder.create();
          EditText editText = dialogView.findViewById(R.id.editText);
          Button okButton = dialogView.findViewById(R.id.validate_url_button);
          okButton.setOnClickListener(
              view2 -> {
                String name = editText.getText().toString();
                if (name.isEmpty()) {
                  editText.setError(this.getResources().getString(R.string.map_create_empty_name));
                } else {
                  okButton.setEnabled(false);
                  okButton.setAlpha(0.5F);
                  this.onDialogPositiveClick(name);
                  dialog.dismiss();
                }
              });
          dialog.show();
        });
    Button joinMapButton = this.findViewById(R.id.join_map);
    joinMapButton.setOnClickListener(
        view -> {
          View dialogView =
              this.getLayoutInflater().inflate(R.layout.activity_enter_sharing_code, null);
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setView(dialogView);
          AlertDialog dialog = builder.create();
          Button okButton = dialogView.findViewById(R.id.validate_code_button);
          okButton.setOnClickListener(
              view2 -> {
                EditText editText = dialogView.findViewById(R.id.code_edit_text);
                String code = editText.getText().toString();
                if (code.length() == 6) {
                  Regex regex = new Regex("[0-9]{6}");
                  if (regex.matches(code)) {
                    okButton.setEnabled(false);
                    okButton.setAlpha(0.5F);
                    this.onCodeDialogPositiveClick(code);
                    dialog.dismiss();
                    return;
                  }
                }
                dialog.show();
              });
        });

    Toolbar toolbar = this.findViewById(R.id.my_toolbar);
    this.setSupportActionBar(toolbar);
    this.checkBattery();
  }

  public boolean onCreateOptionsMenu(Menu menu) {
    this.getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.settings_menu) {
      String playerName = PreferenceManager.getDefaultSharedPreferences(this).getString("name", "");
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      View dialogView = this.getLayoutInflater().inflate(R.layout.activity_enter_name, null);
      builder.setView(dialogView);
      AlertDialog dialog = builder.create();
      EditText editText = dialogView.findViewById(R.id.name_edit_text);
      editText.getText().append(playerName);
      editText.requestFocus();
      InputMethodManager imm =
          (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.showSoftInput(editText, 1);
      Button okButton = dialogView.findViewById(R.id.validate_name_button);
      okButton.setOnClickListener(
          view -> {
            String name = editText.getText().toString();
            if (name.isEmpty()) {
              editText.setError(this.getResources().getString(R.string.map_create_empty_name));
            } else {
              okButton.setEnabled(false);
              okButton.setAlpha(0.5F);
              SharedPreferences.Editor editor =
                  PreferenceManager.getDefaultSharedPreferences(this).edit();
              editor.putString("name", name);
              editor.apply();
              dialog.dismiss();
            }
          });
      dialog.show();
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  protected void onResume() {
    super.onResume();
    this.checkBattery();
  }

  private void onDialogPositiveClick(String name) {
    MapRequest mapRequest =
        new MapRequest(
            name, PreferenceManager.getDefaultSharedPreferences(this).getString("name", "Anonyme"));

    RequestBody body =
        RequestBody.Companion.create(
            this.gson.toJson(mapRequest), MediaType.Companion.parse("application/json"));
    Request request =
        (new Request.Builder()).url(MinitrackConfig.SERVER_URL + "/api/map").post(body).build();
    this.client
        .newCall(request)
        .enqueue(
            new Callback() {
              public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.e("", e.getMessage());
                MainActivity.this.runOnUiThread(
                    () -> {
                      Toast.makeText(
                              MainActivity.this, R.string.map_create_error, Toast.LENGTH_LONG)
                          .show();
                    });
              }

              public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful() && response.body() != null) {
                  ResponseBody responseBody = response.body();
                  String json = responseBody.string();
                  JoinResponse joinResponse =
                      MainActivity.this.gson.fromJson(json, JoinResponse.class);
                  MainActivity.this.runOnUiThread(
                      () -> {
                        Intent intent = new Intent(MainActivity.this, SendLocationActivity.class);
                        SendLocationContextData context =
                            new SendLocationContextData(
                                joinResponse.map().title(),
                                joinResponse.map().key(),
                                joinResponse.playerToken(),
                                false);
                        intent.putExtra("context", context);
                        MainActivity.this.startActivity(intent);
                      });
                } else {
                  Log.e("", String.valueOf(response.code()));
                  MainActivity.this.runOnUiThread(
                      () ->
                          Toast.makeText(
                                  MainActivity.this, R.string.map_create_error, Toast.LENGTH_LONG)
                              .show());
                }
              }
            });
  }

  private void onCodeDialogPositiveClick(String code) {
    JoinRequest joinRequest =
        new JoinRequest(
            code, PreferenceManager.getDefaultSharedPreferences(this).getString("name", "Anonyme"));
    RequestBody.Companion requestCompanion = RequestBody.Companion;
    MediaType mediaType = MediaType.Companion.parse("application/json");
    String requestBody = this.gson.toJson(joinRequest);
    RequestBody body = requestCompanion.create(mediaType, requestBody);
    Request request =
        (new Request.Builder())
            .url(MinitrackConfig.SERVER_URL + "/api/map/join")
            .post(body)
            .build();
    this.client
        .newCall(request)
        .enqueue(
            new Callback() {
              public void onFailure(@NotNull Call call, @NotNull IOException e) {
                MainActivity.this.runOnUiThread(
                    () ->
                        Toast.makeText(
                                MainActivity.this, R.string.map_join_error, Toast.LENGTH_LONG)
                            .show());
              }

              public void onResponse(@NotNull Call call, @NotNull Response response)
                  throws IOException {
                if (response.isSuccessful()) {
                  ResponseBody responseBody = response.body();
                  String json = responseBody != null ? responseBody.string() : null;
                  JoinResponse joinResponse =
                      MainActivity.this.gson.fromJson(json, JoinResponse.class);
                  MainActivity.this.runOnUiThread(
                      () -> {
                        Intent intent = new Intent(MainActivity.this, SendLocationActivity.class);
                        SendLocationContextData context =
                            new SendLocationContextData(
                                joinResponse.map().title(),
                                joinResponse.map().key(),
                                joinResponse.playerToken(),
                                true);
                        intent.putExtra("context", context);
                        MainActivity.this.startActivity(intent);
                      });
                } else {
                  MainActivity.this.runOnUiThread(
                      () ->
                          Toast.makeText(
                                  MainActivity.this, R.string.map_not_found, Toast.LENGTH_LONG)
                              .show());
                }
              }
            });
  }

  private boolean isIgnoringBatteryOptimizations(Context context) {
    PowerManager pwrm =
        (PowerManager) context.getApplicationContext().getSystemService(Context.POWER_SERVICE);
    String name = context.getApplicationContext().getPackageName();
    return pwrm.isIgnoringBatteryOptimizations(name);
  }

  private void checkBattery() {
    Button batteryButton = this.findViewById(R.id.ouvrir_batterie);
    boolean ignoringBattery = this.isIgnoringBatteryOptimizations(this);
    if (!ignoringBattery) {
      batteryButton.setVisibility(View.VISIBLE);
      batteryButton.setOnClickListener(v -> MainActivity.checkBatteryOnClick(this, v));
    } else {
      batteryButton.setVisibility(View.GONE);
    }
  }

  private static void checkBatteryOnClick(MainActivity activity, View it) {
    Toast.makeText(activity.getApplicationContext(), R.string.toast_battery, Toast.LENGTH_LONG)
        .show();
    Intent intent = new Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
    activity.startActivity(intent);
  }

  private void updatePlayerName() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    View dialogView = this.getLayoutInflater().inflate(R.layout.activity_enter_name, null);
    builder.setView(dialogView);
    AlertDialog alertDialog = builder.create();
    EditText editText = dialogView.findViewById(R.id.name_edit_text);
    editText.requestFocus();
    InputMethodManager imm =
        (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
    imm.showSoftInput(editText, 1);
    Button okButton = dialogView.findViewById(R.id.validate_name_button);
    okButton.setOnClickListener(
        view -> {
          String name = editText.getText().toString();
          if (name.isEmpty()) {
            editText.setError(this.getResources().getString(R.string.map_create_empty_name));
          } else {
            okButton.setEnabled(false);
            okButton.setAlpha(0.5F);
            this.preferencesService.updatePlayerName(this, name);
            alertDialog.dismiss();
          }
        });
    alertDialog.show();
  }
}
