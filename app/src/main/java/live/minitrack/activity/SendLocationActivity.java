package live.minitrack.activity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import java.io.IOException;
import java.util.Random;
import kotlin.text.StringsKt;
import live.minitrack.R;
import live.minitrack.service.SendLocationContextData;
import live.minitrack.service.SendLocationService;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

public final class SendLocationActivity extends AppCompatActivity {
  private final OkHttpClient client = new OkHttpClient();
  private SendLocationService sendLocationService;

  private Button startButton;
  private Button finishButton;
  private Button shareButton;

  private WebView webView;

  private String key;
  private String title;
  private String playerToken;

  private boolean fromCode;
  private final String[] images;
  private final ServiceConnection sendLocationServiceConnection;

  public SendLocationActivity() {
    this.images =
        new String[] {
          "alessio_soggetti_gyr9a2cpmhy_unsplash",
          "benjamin_huggett_dfu9ycyatyu_unsplash",
          "brian_erickson_xfnec_rhr48_unsplash",
          "chander_r_z4wh11fmfiq_unsplash",
          "greg_rosenke_e9pdtvfeuji_unsplash",
          "jenny_hill_mqvwb7kuooe_unsplash",
          "josh_gordon_fzhmp6z8oq4_unsplash",
          "marcos_paulo_prado_2csiupeysm0_unsplash"
        };
    this.sendLocationServiceConnection =
        new ServiceConnection() {
          @SuppressLint({"SetJavaScriptEnabled"})
          public void onServiceConnected(ComponentName className, IBinder service) {
            SendLocationService.LocalBinder binder = (SendLocationService.LocalBinder) service;
            Button shareLinkButton = SendLocationActivity.this.findViewById(R.id.share_link_button);
            if (binder.getService().isServiceRunning()) {
              Button startButton = SendLocationActivity.this.startButton;
              startButton.setVisibility(View.GONE);

              Button finishButton = SendLocationActivity.this.finishButton;
              finishButton.setVisibility(View.VISIBLE);

              shareLinkButton.setVisibility(View.VISIBLE);
              WebView webView = SendLocationActivity.this.webView;
              if (webView != null) {
                webView.setWebChromeClient(new WebChromeClient());
              }

              WebSettings webSettings = webView != null ? webView.getSettings() : null;
              if (webSettings != null) {
                webSettings.setJavaScriptEnabled(true);
              }

              webView = SendLocationActivity.this.webView;
              if (webView != null) {
                webView.loadUrl(
                    "https://minitrack.live/map/" + SendLocationActivity.this.key + "/webview");
              }

              webView = SendLocationActivity.this.webView;
              if (webView != null) {
                webView.setVisibility(View.VISIBLE);
              }
            } else {
              startButton.setVisibility(View.VISIBLE);
              finishButton.setVisibility(View.GONE);
            }

            Button shareButton = SendLocationActivity.this.shareButton;
            shareButton.setVisibility(View.VISIBLE);

            CharSequence sharingCode = binder.getService().getSharingCode();
            if (sharingCode != null && !StringsKt.isBlank(sharingCode)) {
              shareButton.setText(binder.getService().getSharingCode());
              shareButton.setEnabled(false);
            }

            SendLocationActivity.this.sendLocationService = binder.getService();
          }

          public void onServiceDisconnected(ComponentName arg0) {
            SendLocationActivity.this.sendLocationService = null;
          }
        };
  }

  @SuppressLint({"SetJavaScriptEnabled"})
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setContentView(R.layout.activity_send_location);
    SendLocationContextData context =
        (SendLocationContextData) this.getIntent().getSerializableExtra("context");
    this.title = context.title();
    this.key = context.key();
    this.playerToken = context.playerToken();
    this.fromCode = context.fromCode();
    Intent serviceIntent = new Intent(this, SendLocationService.class);
    serviceIntent.putExtra("context", context);
    this.bindService(serviceIntent, this.sendLocationServiceConnection, 1);
    ImageView imageView2 = this.findViewById(R.id.sendLocationBackgroundView);
    Random random = new Random();
    int intImage = random.nextInt(this.images.length - 1);
    int id =
        this.getResources().getIdentifier(this.images[intImage], "drawable", this.getPackageName());
    imageView2.setImageResource(id);
    Button shareLinkButton = this.findViewById(R.id.share_link_button);
    TextView mapTitleView = this.findViewById(R.id.map_title);
    mapTitleView.setText(this.title);
    this.startButton = this.findViewById(R.id.start);
    this.shareButton = this.findViewById(R.id.share);
    this.finishButton = this.findViewById(R.id.finishButton);
    this.webView = this.findViewById(R.id.webview);
    if (ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_FINE_LOCATION") != 0
        && ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION")
            != 0) {
      String[] permissions;
      if (VERSION.SDK_INT >= 33) {
        permissions =
            new String[] {
              "android.permission.ACCESS_FINE_LOCATION",
              "android.permission.ACCESS_COARSE_LOCATION",
              "android.permission.POST_NOTIFICATIONS"
            };
        ActivityCompat.requestPermissions(this, permissions, 0);
      } else {
        permissions =
            new String[] {
              "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"
            };
        ActivityCompat.requestPermissions(this, permissions, 0);
      }
    }

    final Button startButton = this.startButton;
    if (startButton != null) {
      startButton.setOnClickListener(
          v -> {
            this.startService(serviceIntent);
            startButton.setVisibility(View.GONE);

            Button finishButton = this.finishButton;
            if (finishButton != null) {
              finishButton.setVisibility(View.VISIBLE);
            }

            shareLinkButton.setVisibility(View.VISIBLE);
            WebView webView = this.webView;
            WebSettings webSettings = webView != null ? webView.getSettings() : null;
            if (webSettings != null) {
              webSettings.setJavaScriptEnabled(true);
            }

            if (webView != null) {
              webView.loadUrl("https://minitrack.live/map/" + this.key + "/webview");
              webView.setVisibility(View.VISIBLE);
            }
          });
    }

    Button finishButton = this.finishButton;
    if (finishButton != null) {
      finishButton.setOnClickListener(
          v -> {
            Intent finishIntent = new Intent(this, SendLocationService.class);
            this.stopService(finishIntent);
            this.finish();
          });
    }

    if (this.fromCode) {
      Button shareButton = this.shareButton;
      if (shareButton != null) {
        shareButton.setVisibility(View.GONE);
      }
    }

    shareLinkButton.setOnClickListener(
        v -> {
          Intent sharingIntent = new Intent("android.intent.action.SEND");
          sharingIntent.setType("text/plain");
          String body = "https://minitrack.live/map/" + this.key;
          sharingIntent.putExtra("android.intent.extra.SUBJECT", this.title);
          sharingIntent.putExtra("android.intent.extra.TEXT", body);
          this.startActivity(
              Intent.createChooser(sharingIntent, this.getString(R.string.share_intent_title)));
        });
    Button shareButton = this.shareButton;
    if (shareButton != null) {
      shareButton.setOnClickListener(
          v -> {
            RequestBody body =
                RequestBody.Companion.create(
                    MediaType.Companion.parse("text/plain"), this.playerToken);
            Request request =
                (new Request.Builder())
                    .url("https://minitrack.live/api/map/" + this.key + "/share")
                    .post(body)
                    .build();
            this.client
                .newCall(request)
                .enqueue(
                    new Callback() {
                      public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        SendLocationActivity.this.runOnUiThread(
                            () ->
                                Toast.makeText(
                                        SendLocationActivity.this,
                                        R.string.sharing_error,
                                        Toast.LENGTH_LONG)
                                    .show());
                      }

                      public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        if (response.body() != null) {
                          String sharingCode = response.body().string();
                          SendLocationActivity.this.runOnUiThread(
                              () -> {
                                SendLocationActivity.this.shareButton.setText(sharingCode);
                                SendLocationActivity.this.shareButton.setEnabled(false);
                                SendLocationActivity.this.sendLocationService.setSharingCode(
                                    sharingCode);
                              });
                        }
                      }
                    });
          });
    }
  }

  public void onRequestPermissionsResult(
      int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    boolean granted = true;

    for (int permissionGranted : grantResults) {
      if (permissionGranted == -1) {
        granted = false;
        break;
      }
    }

    if (granted) {
      Button startButton = this.findViewById(R.id.start);
      startButton.setVisibility(View.VISIBLE);
    }
  }

  public void onBackPressed() {
    super.onBackPressed();
    this.finish();
  }

  public void onDestroy() {
    super.onDestroy();
    this.unbindService(this.sendLocationServiceConnection);
  }
}
